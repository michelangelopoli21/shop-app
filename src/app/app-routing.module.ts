import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROUTES } from './config/routes';

const routes: Routes = [{
  path: '',
  redirectTo: ROUTES.PRODUCT,
  pathMatch: 'full'
},
{
  path: ROUTES.PRODUCT,
  loadChildren: () => import('./pages/products/products.module').then(
    (m) => m.ProductsModule
  ),
},
{
  path: ROUTES.STATISTICS,
  loadChildren: () => import('./pages/statistics/statistics.module').then(
    (m) => m.StatisticsModule
  ),
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
