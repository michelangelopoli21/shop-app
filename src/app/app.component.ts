import { AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { StoreSessionService } from './shared/services/storeSession/storeSession.service';
import { LoaderService } from './shared/services/loader/loader.service';
import { Subject, takeUntil } from 'rxjs';
import { StoreService } from './shared/services/api/store/store.service';
import { StoreDetail } from './shared/services/api/models/Store.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy, AfterViewChecked {

  public storeDetail: StoreDetail;
  public showLoader: boolean;
  private destroy$: Subject<void>;

  constructor(private storeService: StoreService,
    private changeDetectorRef: ChangeDetectorRef,
    private StoreSessionService: StoreSessionService,
    private loaderService: LoaderService) {
    this.storeDetail = {
      employees: [],
      category: '',
      name: ''
    };
    this.showLoader = false;
    this.destroy$ = new Subject();
  }


  ngOnInit(): void {
    this.StoreSessionService.IdStore = 'ijpxNJLM732vm8AeajMR';
    this.activeSubscription();
    this.initializeShop();
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public addAction(): void {
    this.StoreSessionService.addProductAction$.next();
  }

  public statisticsAction(): void {
    this.StoreSessionService.statisticsAction$.next();
  }

  public searchAction(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.StoreSessionService.searchAction$.next(target.value);
  }

  private initializeShop(): void {
    this.storeService.getStoreById(this.StoreSessionService.IdStore).pipe(takeUntil(this.destroy$)).subscribe(res => {
      this.storeDetail = res;
      this.StoreSessionService.Employers = res.employees;
    });
  }

  private activeSubscription(): void {
    this.loaderService.loader$.pipe(takeUntil(this.destroy$)).subscribe(status => this.showLoader = status);
  }

}
