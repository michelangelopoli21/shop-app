
import {
  Component,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
} from '@angular/core';
import { Column } from '../models/columns';

@Component({
  selector: 'app-main-table',
  templateUrl: './main-table.component.html',
  styleUrls: ['./main-table.component.scss'],
})
export class MainTableComponent {
  @Input() desktopTemplate!: TemplateRef<unknown>;
  @Input() mobileTemplate!: TemplateRef<unknown>;

  @Input() rows: Array<{ [key: string]: unknown }>;
  @Input() columns: Array<Column>;

  @Output() changePage: EventEmitter<unknown>;
  constructor() {
    this.changePage = new EventEmitter();
    this.rows = [];
    this.columns = [];
  }

}
