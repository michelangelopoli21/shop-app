
import { Component, Input, TemplateRef } from '@angular/core';
import { Column } from '../models/columns';

@Component({
  selector: 'app-mobile-table',
  templateUrl: './mobile-table.component.html',
  styleUrls: ['./mobile-table.component.scss'],
})
export class MobileTableComponent {
  @Input() template!: TemplateRef<unknown>;
  @Input() rows: Array<{ [key: string]: unknown }>;
  @Input() columns: Array<Column>;

  constructor() {
    this.rows = [];
    this.columns = [];
  }
}
