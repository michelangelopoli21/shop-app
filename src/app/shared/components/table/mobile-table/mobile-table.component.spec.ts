
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileTableComponent } from './mobile-table.component';

describe('MobileTableComponent', () => {
  let component: MobileTableComponent;
  let fixture: ComponentFixture<MobileTableComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MobileTableComponent]
    });
    fixture = TestBed.createComponent(MobileTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
