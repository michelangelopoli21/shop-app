
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebTableComponent } from './web-table.component';

describe('WebTableComponent', () => {
  let component: WebTableComponent;
  let fixture: ComponentFixture<WebTableComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WebTableComponent]
    });
    fixture = TestBed.createComponent(WebTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
