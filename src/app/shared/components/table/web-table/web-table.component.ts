
import { Component, Input, TemplateRef } from '@angular/core';
import { Column } from '../models/columns';

@Component({
  selector: 'app-web-table',
  templateUrl: './web-table.component.html',
  styleUrls: ['./web-table.component.scss'],
})
export class WebTableComponent {
  @Input() template!: TemplateRef<unknown>;
  @Input() rows: Array<{ [key: string]: unknown }>;
  @Input() columns: Array<Column>;

  constructor() {
    this.rows = [];
    this.columns = [];
  }


}
