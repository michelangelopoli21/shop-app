import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-side-panel',
  templateUrl: './side-panel.component.html',
  styleUrls: ['./side-panel.component.scss']
})
export class SidePanelComponent {

  @Input() title: string;
  @Output() close: EventEmitter<void>;

  constructor() {
    this.title = '';
    this.close = new EventEmitter();
  }

  public handleClose(): void {
    this.close.emit();
  }

}
