import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StoreSessionService {

  public addProductAction$: Subject<void>;
  public statisticsAction$: Subject<void>;
  public searchAction$: Subject<string>;

  private employers: Array<string>;
  private idStore: string;


  constructor() {
    this.employers = [];
    this.idStore = '';
    this.addProductAction$ = new Subject();
    this.searchAction$ = new Subject();
    this.statisticsAction$ = new Subject();
  }

  public get Employers(): string[] {
    return this.employers;
  }

  public set Employers(value: string[]) {
    this.employers = value;
  }


  public get IdStore(): string {
    return this.idStore;
  }

  public set IdStore(value: string) {
    this.idStore = value;
  }
}
