export const Path = {
    STORES: '/stores/',
    PRODUCTS: '/products/',
    STATS_CATEGORIES: '/stats/categories/'
}