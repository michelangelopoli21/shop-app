export interface Store {
    id: string;
    data: StoreDetail;
    category: string;
};

export interface StoreDetail {
    name: string;
    category: string;
    employees: Array<string>;
};