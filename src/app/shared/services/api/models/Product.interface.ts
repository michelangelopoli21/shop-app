export interface Product {
    id: string;
    data: ProductDetail;
}

export interface ProductDetail {
    title: string;
    category: string;
    price: number;
    employee: string;
    description: string;
    reviews: Array<string>;
}