import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { StoreDetail } from '../models/Store.interface';
import { environments } from 'src/environments/environment';
import { Path } from '../config';
import { Product, ProductDetail } from '../models/Product.interface';
import { StatisticsCategories } from '../models/Chart.interface';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor(private http: HttpClient) { }

  public getStoreById(id: string): Observable<StoreDetail> {
    return this.http.get<StoreDetail>(`${environments.baseUrl}${Path.STORES}${id}`)
  }

  public getProductsByStoreId(id: string): Observable<Array<Product>> {
    return this.http.get<Array<Product>>(`${environments.baseUrl}${Path.STORES}${id}${Path.PRODUCTS}`);
  }

  public getProduct(idStore: string, idProduct: string): Observable<Product> {
    return this.http.get<Product>(`${environments.baseUrl}${Path.STORES}${idStore}${Path.PRODUCTS}${idProduct}`);
  }

  public deleteProduct(idStore: string, idProduct: string): Observable<boolean> {
    return this.http.delete<boolean>(`${environments.baseUrl}${Path.STORES}${idStore}${Path.PRODUCTS}${idProduct}`);
  }

  public getCategoriesStatistics(idStore: string): Observable<Array<StatisticsCategories>> {
    return this.http.get<Array<StatisticsCategories>>(`${environments.baseUrl}${Path.STORES}${idStore}${Path.STATS_CATEGORIES}`);
  }

  public addNewProduct(idStore: string, payload: ProductDetail): Observable<string> {
    const requestOptions: Object = {
      responseType: 'text'
    }
    return this.http.post<string>(`${environments.baseUrl}${Path.STORES}${idStore}${Path.PRODUCTS}`, payload, requestOptions);
  }
}
