import { MobileTableComponent } from './components/table/mobile-table/mobile-table.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './components/loader/loader.component';
import { MainTableComponent } from './components/table/main-table/main-table.component';
import { WebTableComponent } from './components/table/web-table/web-table.component';
import { HeaderComponent } from './components/header/header.component';
import { SidePanelComponent } from './components/side-panel/side-panel.component';

@NgModule({
  declarations: [
    LoaderComponent,
    MainTableComponent,
    MobileTableComponent,
    WebTableComponent,
    HeaderComponent,
    SidePanelComponent
  ],
  imports: [CommonModule],
  exports: [LoaderComponent, MainTableComponent, HeaderComponent, SidePanelComponent],
})
export class SharedModule { }
