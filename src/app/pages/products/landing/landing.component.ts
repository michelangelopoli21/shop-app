import { StoreSessionService } from '../../../shared/services/storeSession/storeSession.service';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject, take, takeUntil, debounceTime } from 'rxjs';
import { Column } from 'src/app/shared/components/table/models/columns';
import { Product, ProductDetail } from 'src/app/shared/services/api/models/Product.interface';
import { StoreService } from 'src/app/shared/services/api/store/store.service';
import { AddProductComponent } from '../components/add-product/add-product.component';
import { ProductReviewsComponent } from '../components/product-reviews/product-reviews.component';
import { DeleteProductComponent } from '../components/delete-product/delete-product.component';
import { StatisticsComponent } from '../components/statistics/statistics.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit, OnDestroy {

  @ViewChild('addProduct') addProduct: AddProductComponent;
  @ViewChild('productReview') appProductReview: ProductReviewsComponent;
  @ViewChild('deleteProduct') appDeleteProduct: DeleteProductComponent;
  @ViewChild('stats') appStatistics: StatisticsComponent;


  public products: Product[];
  public columns: Column[];

  private searched: string;
  private destroy$: Subject<void>;


  constructor(private storeService: StoreService, private storeSessionService: StoreSessionService, private toastr: ToastrService) {
    this.products = [];
    this.columns = [];
    this.destroy$ = new Subject();
    this.searched = '';
  }

  ngOnInit(): void {
    this.initializeColumns();
    this.getProducts();
    this.activeSubscriptions();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public get Products(): Product[] {
    return this.searched.length > 2 ?
      this.products.filter(item => item.data.title.toUpperCase().includes(this.searched.toUpperCase())) :
      this.products;
  }


  public getProducts(): void {
    this.storeService.getProductsByStoreId(this.storeSessionService.IdStore).pipe(takeUntil(this.destroy$)).subscribe(response => {
      this.products = response;
    });
  }

  public openDeleteModal(data: Product): void {
    this.closePanels();
    this.appDeleteProduct.openPanel(data);
  }

  public openProductReviews(data: ProductDetail): void {
    this.closePanels();
    this.appProductReview.openPanel(data);
  }

  public handleDelete(id: string): void {
    this.storeService.deleteProduct(this.storeSessionService.IdStore, id).pipe(take(1)).subscribe(() => {
      const index = this.products.findIndex(item => item.id === id);
      if (index === -1) { return; }
      this.products.splice(index, 1);
      this.products = [...this.products];
      this.toastr.success('Eliminazione', 'Prodotto eliminato con successo');
    });
  }

  private openAddProduct(): void {
    this.closePanels();
    this.addProduct.openPanel();
  }

  private openStatistics(): void {
    this.closePanels();
    this.appStatistics.openPanel();
  }

  private closePanels(): void {
    this.addProduct.closePanel();
    this.appDeleteProduct.closePanel();
    this.appProductReview.closePanel();
  }


  private activeSubscriptions(): void {
    this.storeSessionService.addProductAction$.pipe(takeUntil(this.destroy$)).subscribe(() => this.openAddProduct());
    this.storeSessionService.searchAction$.pipe(takeUntil(this.destroy$), debounceTime(700)).subscribe((searched) => this.searched = searched);
    this.storeSessionService.statisticsAction$.pipe(takeUntil(this.destroy$)).subscribe((searched) => this.openStatistics());
  }

  private initializeColumns(): void {
    this.columns = [
      {
        label: 'Titolo',
        prop: 'title'
      },
      {
        label: 'Descrizione',
        prop: 'description'
      },
      {
        label: 'Categoria',
        prop: 'category'
      },
      {
        label: 'Prezzo',
        prop: 'price'
      },
      {
        label: 'Impiegato',
        prop: 'employee'
      },
      {
        label: 'Azioni',
        prop: 'actions'
      },
    ]
  }

}
