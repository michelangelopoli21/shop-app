import { NgChartsModule } from 'ng2-charts';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsRoutingModule } from './products-routing.module';
import { LandingComponent } from './landing/landing.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AddProductComponent } from './components/add-product/add-product.component';

import { ProductReviewsComponent } from './components/product-reviews/product-reviews.component';
import { DeleteProductComponent } from './components/delete-product/delete-product.component';
import { FormsModule } from '@angular/forms';
import { StatisticsComponent } from './components/statistics/statistics.component';


@NgModule({
  declarations: [
    LandingComponent,
    AddProductComponent,
    ProductReviewsComponent,
    DeleteProductComponent,
    StatisticsComponent,
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    SharedModule,
    FormsModule,
    NgChartsModule
  ],
})
export class ProductsModule { }
