import { StoreService } from 'src/app/shared/services/api/store/store.service';
import { StoreSessionService } from '../../../../shared/services/storeSession/storeSession.service';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ProductDetail } from 'src/app/shared/services/api/models/Product.interface';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  @ViewChild('productForm') appForm: NgForm;

  @Output() afterSubmit: EventEmitter<void>;

  public showPanel: boolean;
  public product: ProductDetail;
  public employersDomain: Array<string>;

  constructor(private StoreSessionService: StoreSessionService, private storeService: StoreService, private toastr: ToastrService) {
    this.product = {
      reviews: [],
      category: '',
      title: '',
      description: '',
      employee: '',
      price: undefined
    };
    this.afterSubmit = new EventEmitter();
  }

  ngOnInit(): void {
    this.employersDomain = this.StoreSessionService.Employers;
  }

  public openPanel(): void {
    this.showPanel = true;
  }

  public closePanel(): void {
    this.resetProduct();
    this.showPanel = false;
  }

  public addNewProduct(): void {
    if (this.appForm.invalid) {
      return;
    }
    this.storeService.addNewProduct(this.StoreSessionService.IdStore, this.product).subscribe(() => {
      this.closePanel();
      this.afterSubmit.emit();
      this.toastr.success('Nuovo Prodotto', 'Prodotto creato con successo');
    });
  }

  private resetProduct(): void {
    this.product = {
      reviews: [],
      category: '',
      title: '',
      description: '',
      employee: '',
      price: undefined
    };
  }

}
