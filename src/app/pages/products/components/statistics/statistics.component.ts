import { Subject, takeUntil } from 'rxjs';
import { StoreService } from 'src/app/shared/services/api/store/store.service';
import { Component, OnDestroy } from '@angular/core';
import { StoreSessionService } from 'src/app/shared/services/storeSession/storeSession.service';
import { ChartData } from 'chart.js';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnDestroy {

  public showPanel: boolean;
  public title: string;
  public reviews: Array<string>;
  public polarAreaChartData: ChartData<'polarArea'> = {
    labels: [],
    datasets: [
      {
        data: [],
        label: 'Numero di prodotti',
      },
    ],
  };
  private destroy$: Subject<void>;

  constructor(private storeService: StoreService, private storeSessionService: StoreSessionService) {
    this.showPanel = false;
    this.title = '';
    this.reviews = [];
    this.destroy$ = new Subject();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public openPanel(): void {
    this.showPanel = true;
    this.getStatistics();
  }

  public closePanel(): void {
    this.showPanel = false;
  }

  private getStatistics(): void {
    this.storeService.getCategoriesStatistics(this.storeSessionService.IdStore).pipe(takeUntil(this.destroy$)).subscribe(response => {
      this.polarAreaChartData.labels = response.map(item => item.category);
      this.polarAreaChartData.datasets[0].data = response.map(item => item.numberOfProducts);
      this.polarAreaChartData = { ...this.polarAreaChartData };
    });
  }
}
