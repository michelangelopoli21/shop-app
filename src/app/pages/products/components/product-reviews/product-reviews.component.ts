import { Component } from '@angular/core';
import { ProductDetail } from 'src/app/shared/services/api/models/Product.interface';

@Component({
  selector: 'app-product-reviews',
  templateUrl: './product-reviews.component.html',
  styleUrls: ['./product-reviews.component.scss']
})
export class ProductReviewsComponent {
  public showPanel: boolean;
  public title: string;
  public reviews: Array<string>;

  constructor() {
    this.showPanel = false;
    this.title = '';
    this.reviews = [];
  }

  public openPanel(data: ProductDetail): void {
    this.title = data.title;
    this.showPanel = true;
    this.reviews = data.reviews ? data.reviews : [];
  }

  public closePanel(): void {
    this.showPanel = false;
  }
}
