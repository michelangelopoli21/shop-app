import { Component, EventEmitter, Output } from '@angular/core';
import { Product } from 'src/app/shared/services/api/models/Product.interface';

@Component({
  selector: 'app-delete-product',
  templateUrl: './delete-product.component.html',
  styleUrls: ['./delete-product.component.scss']
})
export class DeleteProductComponent {

  @Output() confirm: EventEmitter<string>;

  public showPanel: boolean;
  public data: Product;

  constructor() {
    this.data = {
      id: '',
      data: {
        reviews: [],
        category: '',
        title: '',
        description: '',
        employee: '',
        price: null
      }
    };
    this.confirm = new EventEmitter();
  }

  public openPanel(data: Product): void {
    this.data = data;
    this.showPanel = true;
  }

  public closePanel(): void {
    this.showPanel = false;
  }

  public handleConfirm(): void {
    this.confirm.emit(this.data.id);
    this.closePanel();
  }

}
